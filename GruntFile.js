'use strict';

module.exports = function(grunt) {
    require('time-grunt')(grunt);
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        app: {},

        connect: {
            server: {
                options: {
                    port: 3001,
                    base: '.'
                }
            }
        },
        
        // Syncs up browsers and handles livereloading like a boss
        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        'index.html',
                        'dist/css/main.min.css',
                        'dist/js/main.min.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: '.'
                }
            }
        },

        // combine JS
        concat: {
            vendorjs: {
                src: [
                    'assets/bower/angular/angular.js',
                    'assets/bower/angular-preload-image/angular-preload-image.js'
                ],
                dest: 'dist/js/vendor.min.js',
                nonull: true
            },       
            // Concats all .js files found in app/js
            mainjs: {
                src: ['assets/js/app.js'],
                dest: 'dist/js/main.min.js',
                nonull: true
            }
        },

        // compress any/all .js files found in /js.
        uglify: {
            options: {
                mangle: true,
                compress: true
            },
            dist: {
                files: {
                    'dist/js/vendor.min.js': ['dist/js/vendor.min.js'],
                    'dist/js/main.min.js': ['dist/js/main.min.js']
                }
            }
        },

        // minify CSS
        cssmin: {
            dist: {
                files: [{
                        expand: true,
                        cwd: 'dist/css',
                        src: 'main.min.css',
                        dest: 'dist/css'
                    }
                ]
            }
        },

        // automatically watch and rerun
        watch: {
            js: {
                files: ['assets/js/**/*.js'],
                tasks: ['concat', 'uglify']
            },
            sass: {
                files: ['assets/css/**/*.{scss,sass}'],
                tasks: ['css']
            }
        },

        // Compiles Sass to CSS and generates necessary files if requested
        sass: {
            options: {
                loadPath: 'css'
            },
            dist: {
                options: {
                    outputStyle: 'expanded'
                },
                files: [{
                  expand: true,
                  cwd: 'assets/css',
                  src: ['main.scss'],
                  dest: 'dist/css',
                  ext: '.min.css'
                }]
            }
        },
    });

    grunt.registerTask('serve', ['connect', 'sync']);
    grunt.registerTask('sync', ['browserSync', 'watch']);

    grunt.registerTask('js', ['concat', 'uglify:dist']);
    grunt.registerTask('css', ['sass', 'cssmin']);

    grunt.registerTask('build', ['css', 'js']);
    grunt.registerTask('run', ['build',  'serve']);
    grunt.registerTask('default', ['build']);
};