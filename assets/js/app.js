(function(){
     "use strict";

    angular.module('honeybookApp', ['angular-preload-image'])
    .constant('API_URL', 'https://candidate-test.herokuapp.com/contacts')
    .service('honeybookContacts', ['$http', 'API_URL', function($http, API_URL){
        var model = this;
        model.contacts = [];
        model.loading = false;

        model.getContacts = function(){
            model.loading = true;
            $http({
                method  : 'GET',
                url     : API_URL
            })
            .then(function(response){
                model.contacts = response.data;
                model.loading = false;
            }, function(response){
                model.loading = false;
            });
        };
    }])
    .controller('honeybookCtrl', ['$scope', 'honeybookContacts', function($scope, honeybookContacts) {
        $scope.contactsModel = honeybookContacts;
        $scope.contactsModel.getContacts();
    }]);

})();